

function gcd(a, b) {
    if (b == 0)
        return a;
    return gcd(b, a % b);
}

function lcm(arr) {
    let n=arr.length;
    let ans = arr[0];

    for (let i = 1; i < n; i++)
        ans = (((arr[i] * ans)) /
            (gcd(arr[i], ans)));

    return ans;
}


console.log(lcm([1, 2, 3, 4, 5, 6, 7, 8, 9]));
console.log(lcm([5]));
console.log(lcm([5, 7, 11]));
console.log(lcm([5, 7, 11, 35, 55, 77]));