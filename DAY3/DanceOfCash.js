const MOVES = ["Shimmy", "Shake", "Pirouette", "Slide", "Box Step", "Headspin", "Dosado", "Pop", "Lock", "Arabesque"];

function danceConvert(pin) {
    let result = [];
    function isNumeric(n) {
        return !isNaN(parseFloat(n)) && isFinite(n);
    }
    if( isNumeric(pin) && pin.length == 4) {
        const arr = pin.split("").map(i=>Number(i));
        for (i = 0; i < arr.length; i++) {
            value = arr[i] + i;
            value >= 10 ?
                result.push(MOVES[value % 10])
                : result.push(MOVES[value]);
        }
        return result;
    }
    else
     return("invalid!!!")
    
}
console.log(danceConvert("9999"));
console.log(danceConvert("0000"));
console.log(danceConvert("3856"));
console.log(danceConvert("32a1"));