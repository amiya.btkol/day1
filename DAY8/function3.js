function product(a,b) {
   return function(a1,b1){
       return function(a2,b2){
           return((a*a1*a2)+(b*b1*b2))
       }
   }


}

console.log(product(1, 2)(1, 1)(2, 3))
console.log(product(10, 2)(5, 0)(2, 3));
console.log(product(1, 2)(2, 3)(3, 4));
console.log(product(1, 2)(0, 3)(3, 0));