
// Promises III: Native Promise, Introducing the Executor
let promise = new Promise(function (resolve, reject) {
    setTimeout(() => resolve("Promis work"), 1000); 
   
});

promise.then(
    result => console.log(result), 
    error => console.log(error)
);